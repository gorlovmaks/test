<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 05.09.18
 * Time: 11:35
 */

class Router
{

//    public function __construct()
//    {
//        echo 'Привет мир';
//    }

    /**array of routes
     * @var array
     */
    protected static $routes = [];
    /**current of route
     * @var array
     */
    protected static $route = [];

    /** add route in array of routes
     * @param string $regexp regular expression route
     * @param array $route ([controller, action, params])
     */
    public static function add($regexp, $route)
    {
        self::$routes[$regexp] = $route;
    }

    /**Returns all routes
     *
     * @return array $routes
     */
    public static function getRoutes()
    {
        return self::$routes;
    }

    /**Return current route
     *
     * @return array $route
     */
    public static function getRoute()
    {
        return self::$route;
    }

    /**is seeking coincidence routes
     * @param string $url Route received by the request
     * @return bool
     */
    public static function matchRoute($url)
    {
        foreach (self::$routes as $pattern => $route) {
            if (preg_match("#$pattern#i", $url, $matches)) {

                foreach ($matches as $key => $value) {
                    if (is_string($key)) {
                        $route[$key] = $value;
                    }
                }
                self::$route = $route;
                print_r($route);
                return true;
            }
        }
        return false;
    }

    public static function dispatch($url)
    {

        if (self::matchRoute($url)) {
            $controller = Router::$route['controller'];
            if (class_exists($controller)) {
                echo 'Ок';
            } else {
                echo 'Контроллер не '.$controller.' найден';
            }
        } else {
            http_response_code(404);
            include '404.html';
        }
    }
}